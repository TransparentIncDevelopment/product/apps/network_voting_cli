/// Display struct for printing to stdout / stderr in Network Voting CLI
pub mod cli_display;
/// Display models with constructed messaging and formatting data
pub mod models;
/// Inline spinner for indicating async actions
pub mod spinner;
/// Tables to be printed as the result of some command, possibly using multiple models
pub mod tables;
