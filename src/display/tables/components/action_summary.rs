use crate::display::models::action::ActionViewModel;
use crate::display::models::address::AddressViewModel;
use crate::display::tables::components::address_display::{
    AddressDisplayComponent, AddressDisplayComponentProps,
};
use crate::display::tables::components::StringComponent;
use std::fmt::{Display, Formatter};

pub struct ActionSummaryComponentProps {
    pub action: ActionViewModel,
    pub truncate_addresses: bool,
}

pub struct ActionSummaryComponent {
    props: ActionSummaryComponentProps,
}

impl ActionSummaryComponent {
    pub fn new(props: ActionSummaryComponentProps) -> Self {
        ActionSummaryComponent { props }
    }

    fn get_address_component(&self, address: &AddressViewModel) -> AddressDisplayComponent {
        AddressDisplayComponent::new(AddressDisplayComponentProps {
            truncate: self.props.truncate_addresses,
            address: address.clone(),
            string_color: None,
        })
    }
}

impl StringComponent for ActionSummaryComponent {}

impl Display for ActionSummaryComponent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.props.action.clone() {
            ActionViewModel::AddMember {
                member,
                encryption_key,
            } => {
                write!(
                    f,
                    "Add Member {} with public encryption key {}",
                    self.get_address_component(&member),
                    encryption_key
                )
            }
            ActionViewModel::RemoveMember { member } => {
                write!(f, "Remove Member {}", self.get_address_component(&member))
            }
            ActionViewModel::SetTrust {
                trust,
                encryption_key,
            } => {
                write!(
                    f,
                    "Set Trust {} with public encryption key {}",
                    self.get_address_component(&trust),
                    encryption_key
                )
            }
            ActionViewModel::SetLimitedAgent { limited_agent } => match limited_agent {
                Some(address) => write!(
                    f,
                    "Set Limited Agent {}",
                    self.get_address_component(&address)
                ),
                None => write!(f, "Remove Limited Agent"),
            },
            ActionViewModel::AddValidator { validator } => write!(
                f,
                "Add Validator {}",
                self.get_address_component(&validator)
            ),
            ActionViewModel::RemoveValidator { validator } => write!(
                f,
                "Remove Validator {}",
                self.get_address_component(&validator)
            ),
            ActionViewModel::AllowlistCidrBlock {
                cidr_block,
                address,
            } => write!(
                f,
                "Allowlist {} for {}",
                &cidr_block,
                self.get_address_component(&address)
            ),
            ActionViewModel::RemoveAllowlist {
                cidr_block,
                address,
            } => write!(
                f,
                "Remove Allowlist for {} for {}",
                &cidr_block,
                self.get_address_component(&address)
            ),
            // temporarily disabled for ADO 6984

            // ActionViewModel::SetValidatorEmissionRate {
            //     block_quota,
            //     minor_units_per_emission,
            // } => match block_quota {
            //     1 => write!(
            //         f,
            //         "Set validator emission rate to {} minor units per block",
            //         minor_units_per_emission,
            //     ),
            //     _ => write!(
            //         f,
            //         "Set validator emission rate to {} minor units per {} blocks",
            //         minor_units_per_emission, block_quota
            //     ),
            // },
            ActionViewModel::Invalid => write!(f, "Invalid or Unsupported Action"),
        }
    }
}

// temporarily disabled for ADO 6984

// #[cfg(test)]
// mod tests {
//     #![allow(non_snake_case)]
//     use super::{ActionSummaryComponent, ActionSummaryComponentProps};

//     #[test]
//     fn set_validator_emission_rate__displays_single_block_quota() {
//         // Given
//         let component = ActionSummaryComponent::new(ActionSummaryComponentProps {
//             truncate_addresses: false,
//             action: crate::display::models::action::ActionViewModel::SetValidatorEmissionRate {
//                 block_quota: 1,
//                 minor_units_per_emission: 17,
//             },
//         });

//         // When
//         let str = format!("{}", component);

//         // Then
//         assert_eq!(
//             str,
//             "Set validator emission rate to 17 minor units per block"
//         );
//     }

//     #[test]
//     fn set_validator_emission_rate__displays_multiple_block_quota() {
//         // Given
//         let component = ActionSummaryComponent::new(ActionSummaryComponentProps {
//             truncate_addresses: false,
//             action: crate::display::models::action::ActionViewModel::SetValidatorEmissionRate {
//                 block_quota: 3,
//                 minor_units_per_emission: 17,
//             },
//         });

//         // When
//         let str = format!("{}", component);

//         // Then
//         assert_eq!(
//             str,
//             "Set validator emission rate to 17 minor units per 3 blocks"
//         );
//     }

//     #[test]
//     fn set_validator_emission_rate__displays_zero_minor_units_per_one_block() {
//         // Given
//         let component = ActionSummaryComponent::new(ActionSummaryComponentProps {
//             truncate_addresses: false,
//             action: crate::display::models::action::ActionViewModel::SetValidatorEmissionRate {
//                 block_quota: 1,
//                 minor_units_per_emission: 0,
//             },
//         });

//         // When
//         let str = format!("{}", component);

//         // Then
//         assert_eq!(
//             str,
//             "Set validator emission rate to 0 minor units per block"
//         );
//     }
// }
