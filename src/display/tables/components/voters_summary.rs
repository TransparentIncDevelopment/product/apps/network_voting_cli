#[cfg(test)]
mod tests;

use crate::display::cli_display::PrintableTable;
use crate::display::models::address::AddressViewModel;
use crate::display::models::votes::VotesViewModel;
use crate::display::tables::components::address_display::{
    AddressDisplayComponent, AddressDisplayComponentProps,
};
use colored::Color;
use prettytable::{format, Row, Table};

pub const NO_NAY_VOTERS_MESSAGE: &str = "No Nay votes";
pub const NO_YEA_VOTERS_MESSAGE: &str = "No Yea votes";

pub struct VotesSummaryComponent {
    props: VotesSummaryComponentProps,
}

pub struct VotesSummaryComponentProps {
    pub votes: VotesViewModel,
    pub truncate_addresses: bool,
}

impl VotesSummaryComponent {
    pub fn new(props: VotesSummaryComponentProps) -> Self {
        Self { props }
    }

    fn create_voters_string(&self, voters: &[AddressViewModel], color: Option<Color>) -> String {
        let voters_strings = voters
            .iter()
            .map(|address| {
                AddressDisplayComponent::new(AddressDisplayComponentProps {
                    truncate: self.props.truncate_addresses,
                    address: address.clone(),
                    string_color: color,
                })
                .to_string()
            })
            .collect::<Vec<String>>();
        voters_strings.join("\n")
    }

    fn create_yea_votes_row(&self) -> Row {
        let message = if self.props.votes.yea_voters.count == 0 {
            NO_YEA_VOTERS_MESSAGE.to_string()
        } else {
            self.create_voters_string(&self.props.votes.yea_voters.addresses, None)
        };
        row![b->"Yea Votes", message]
    }

    fn create_nay_votes_row(&self) -> Row {
        let message = if self.props.votes.nay_voters.count == 0 {
            NO_NAY_VOTERS_MESSAGE.to_string()
        } else {
            self.create_voters_string(&self.props.votes.nay_voters.addresses, None)
        };
        row![b->"Nay Votes", message]
    }
}

impl PrintableTable for VotesSummaryComponent {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.add_row(self.create_yea_votes_row());
        table.add_row(row![""]);
        table.add_row(self.create_nay_votes_row());
        table
    }
}
