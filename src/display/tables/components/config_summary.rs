use prettytable::{format, Row, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::config::CliConfigViewModel;
use crate::display::tables::components::address_display::{
    AddressDisplayComponent, AddressDisplayComponentProps,
};
use crate::display::tables::components::TableComponent;
use prettytable::format::TableFormat;

pub const NOT_SET_MESSAGE: &str = "Value not set";

pub struct ConfigSummaryComponentProps {
    pub config: CliConfigViewModel,
    pub table_format: Option<TableFormat>,
    pub truncate_addresses: bool,
}

pub struct ConfigSummaryComponent {
    props: ConfigSummaryComponentProps,
}
impl TableComponent for ConfigSummaryComponent {}

impl ConfigSummaryComponent {
    pub fn new(props: ConfigSummaryComponentProps) -> Self {
        ConfigSummaryComponent { props }
    }

    pub fn create_url_row(&self) -> Row {
        row![b->"URL", self.props.config.url.clone().unwrap_or_else(|| NOT_SET_MESSAGE.to_string())]
    }

    pub fn create_issuer_row(&self) -> Row {
        let issuer_label_cell = cell!("Issuer");
        match self.props.config.issuer.clone() {
            Some(issuer) => {
                let issuer_display_component =
                    AddressDisplayComponent::new(AddressDisplayComponentProps {
                        truncate: self.props.truncate_addresses,
                        address: issuer,
                        string_color: None,
                    });
                row![b->issuer_label_cell, issuer_display_component]
            }
            None => row![b->issuer_label_cell, NOT_SET_MESSAGE],
        }
    }

    pub fn create_jwt_row(&self) -> Row {
        row![b->"JWT", self.props.config.jwt.clone().unwrap_or_else(|| NOT_SET_MESSAGE.to_string())]
    }
}

impl PrintableTable for ConfigSummaryComponent {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(
            self.props
                .table_format
                .unwrap_or(*format::consts::FORMAT_CLEAN),
        );
        table.add_row(self.create_url_row());
        table.add_row(self.create_issuer_row());
        table.add_row(self.create_jwt_row());
        table
    }
}
