use crate::display::models::address::AddressViewModel;
use crate::display::tables::components::StringComponent;
use colored::*;
use std::fmt::{Display, Formatter};

pub struct AddressDisplayComponentProps {
    pub address: AddressViewModel,
    pub truncate: bool,
    pub string_color: Option<Color>,
}

pub struct AddressDisplayComponent {
    props: AddressDisplayComponentProps,
}

impl AddressDisplayComponent {
    pub fn new(props: AddressDisplayComponentProps) -> Self {
        AddressDisplayComponent { props }
    }

    fn get_address_string(&self) -> String {
        if self.props.truncate {
            return self.props.address.truncated_address.clone();
        };
        self.props.address.full_address.clone()
    }
}

impl StringComponent for AddressDisplayComponent {}

impl Display for AddressDisplayComponent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.props.string_color {
            Some(c) => write!(f, "{}", self.get_address_string().as_str().color(c)),
            None => write!(f, "{}", self.get_address_string()),
        }
    }
}
