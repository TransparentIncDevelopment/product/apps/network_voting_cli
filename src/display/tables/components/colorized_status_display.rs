use crate::display::models::status::StatusViewModel;
use colored::Colorize;
use std::fmt::{Display, Formatter};

pub struct ColorizedStatusComponent {
    pub status: StatusViewModel,
}

impl Display for ColorizedStatusComponent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.status.0.as_str() {
            "Accepted" => write!(f, "{}", self.status.0.as_str().color("green")),
            "Rejected" => write!(f, "{}", self.status.0.as_str().color("red")),
            _ => write!(f, "{}", self.status.0),
        }
    }
}
