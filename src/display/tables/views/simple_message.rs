use prettytable::{format, Table};

use crate::display::cli_display::PrintableTable;

pub struct SimpleMessageTableProps {
    pub title: String,
    pub message: String,
}

pub struct SimpleMessageTable {
    props: SimpleMessageTableProps,
}

impl SimpleMessageTable {
    pub fn new(props: SimpleMessageTableProps) -> Self {
        SimpleMessageTable { props }
    }
}

impl PrintableTable for SimpleMessageTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_DEFAULT);
        table.add_row(row![bc=>self.props.title]);
        table.add_row(row![c=>self.props.message]);
        table
    }
}
