use crate::cli::models::action::Action as ActionData;
use crate::display::models::address::AddressViewModel;

#[derive(Clone, Debug)]
pub enum ActionViewModel {
    Invalid,
    AddMember {
        member: AddressViewModel,
        encryption_key: String,
    },
    RemoveMember {
        member: AddressViewModel,
    },
    SetTrust {
        trust: AddressViewModel,
        encryption_key: String,
    },
    SetLimitedAgent {
        limited_agent: Option<AddressViewModel>,
    },
    AddValidator {
        validator: AddressViewModel,
    },
    RemoveValidator {
        validator: AddressViewModel,
    },
    AllowlistCidrBlock {
        cidr_block: String,
        address: AddressViewModel,
    },
    RemoveAllowlist {
        cidr_block: String,
        address: AddressViewModel,
    },
    // temporarily disabled for ADO 6984

    // SetValidatorEmissionRate {
    //     minor_units_per_emission: u64,
    //     block_quota: u32,
    // },
}

impl From<Option<ActionData>> for ActionViewModel {
    fn from(action_opt: Option<ActionData>) -> Self {
        match action_opt {
            Some(a) => a.into(),
            None => ActionViewModel::Invalid,
        }
    }
}

impl From<ActionData> for ActionViewModel {
    fn from(action: ActionData) -> Self {
        match action {
            ActionData::AddMember {
                address,
                encryption_key,
            } => ActionViewModel::AddMember {
                member: address.into(),
                encryption_key,
            },
            ActionData::RemoveMember { address } => ActionViewModel::RemoveMember {
                member: address.into(),
            },
            ActionData::SetTrust {
                address,
                encryption_key,
            } => ActionViewModel::SetTrust {
                trust: address.into(),
                encryption_key,
            },
            ActionData::AddValidator { address } => ActionViewModel::AddValidator {
                validator: address.into(),
            },
            ActionData::RemoveValidator { address } => ActionViewModel::RemoveValidator {
                validator: address.into(),
            },
            ActionData::AllowlistCidrBlock {
                cidr_block,
                address,
            } => ActionViewModel::AllowlistCidrBlock {
                cidr_block,
                address: address.into(),
            },
            ActionData::RemoveAllowlist {
                cidr_block,
                address,
            } => ActionViewModel::RemoveAllowlist {
                cidr_block,
                address: address.into(),
            },
            ActionData::SetLimitedAgent { address } => ActionViewModel::SetLimitedAgent {
                limited_agent: address.map(Into::into),
            },
            ActionData::SetCode { .. } => ActionViewModel::Invalid,
            // temporarily disabled for ADO 6984

            // ActionData::SetValidatorEmissionRate {
            //     minor_units_per_emission,
            //     block_quota,
            // } => ActionViewModel::SetValidatorEmissionRate {
            //     minor_units_per_emission,
            //     block_quota,
            // },
        }
    }
}
