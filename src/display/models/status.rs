use crate::cli::models::status::Status;

// Note: Broken out to include possibility of granular status display info in future beyond string
#[derive(Clone, Debug)]
pub struct StatusViewModel(pub String);

impl From<Status> for StatusViewModel {
    fn from(s: Status) -> Self {
        StatusViewModel(s.to_string())
    }
}
