use crate::cli::models::votes::VotesData;
use crate::display::models::address::AddressesViewModel;

#[derive(Clone, Debug)]
pub struct VotesViewModel {
    pub yea_voters: AddressesViewModel,
    pub nay_voters: AddressesViewModel,
}

impl From<VotesData> for VotesViewModel {
    fn from(votes: VotesData) -> Self {
        Self {
            yea_voters: votes.yea_voters.into(),
            nay_voters: votes.nay_voters.into(),
        }
    }
}
