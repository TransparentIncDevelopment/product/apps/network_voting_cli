#[cfg(test)]
mod tests;

use unicode_truncate::UnicodeTruncateStr;

const ADDRESS_MAX_CHAR: usize = 7;

#[derive(Clone, Debug)]
pub struct AddressesViewModel {
    pub addresses: Vec<AddressViewModel>,
    pub count: usize,
}

#[derive(Clone, Debug)]
pub struct AddressViewModel {
    pub full_address: String,
    pub truncated_address: String,
}

impl From<String> for AddressViewModel {
    fn from(s: String) -> Self {
        let t = s.clone();
        let (t, _) = t.unicode_truncate(ADDRESS_MAX_CHAR);
        AddressViewModel {
            full_address: s,
            truncated_address: format!("{}...", t),
        }
    }
}

impl From<&String> for AddressViewModel {
    fn from(s: &String) -> Self {
        s.clone().into()
    }
}

impl From<Vec<String>> for AddressesViewModel {
    fn from(v_s: Vec<String>) -> Self {
        let addresses = v_s
            .iter()
            .map(|s| s.into())
            .collect::<Vec<AddressViewModel>>();
        let count = addresses.len();
        AddressesViewModel { addresses, count }
    }
}
