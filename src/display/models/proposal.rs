use crate::cli::models::proposal::ProposalData;
use crate::display::models::action::ActionViewModel;
use crate::display::models::address::AddressViewModel;
use crate::display::models::status::StatusViewModel;
use crate::display::models::votes::VotesViewModel;

#[derive(Clone, Debug)]
pub struct ProposalsViewModel(pub Vec<ProposalViewModel>);

#[derive(Clone, Debug)]
pub struct ProposalViewModel {
    pub id: u32,
    pub action: ActionViewModel,
    pub member_votes: VotesViewModel,
    pub validator_votes: VotesViewModel,
    pub status: StatusViewModel,
    pub proposer: AddressViewModel,
    pub expires: String,
}

impl From<ProposalData> for ProposalViewModel {
    fn from(prop: ProposalData) -> Self {
        (&prop).into()
    }
}

impl From<&ProposalData> for ProposalViewModel {
    fn from(prop: &ProposalData) -> Self {
        ProposalViewModel {
            id: prop.id,
            action: prop.action.clone().into(),
            member_votes: prop.member_votes.clone().into(),
            validator_votes: prop.validator_votes.clone().into(),
            status: prop.status.into(),
            proposer: prop.proposer.clone().into(),
            expires: prop.expires.to_string(),
        }
    }
}

impl From<Vec<ProposalData>> for ProposalsViewModel {
    fn from(props: Vec<ProposalData>) -> Self {
        let display_props = props
            .iter()
            .map(|prop| prop.into())
            .collect::<Vec<ProposalViewModel>>();
        ProposalsViewModel(display_props)
    }
}
