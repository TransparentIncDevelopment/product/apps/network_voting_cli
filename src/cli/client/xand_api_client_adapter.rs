use std::{
    convert::{TryFrom, TryInto},
    time::Duration,
};

use futures::{try_join, StreamExt};
use snafu::ResultExt;
use url::Url;
use xand_address::Address;
use xand_api_client::{
    errors::XandApiClientError, models::TransactionUpdate, TransactionStatus,
    TransactionStatusStream, XandApiClient, XandApiClientTrait,
};

use crate::cli::client::xand_api_client_adapter::data_model_translations::proposal::build_proposal_data;
use crate::cli::client::xand_api_client_adapter::data_model_translations::votes::Participants;
use crate::cli::client::Client;
use crate::cli::interface::vote::Vote;
use crate::cli::models::action::Action;
use crate::cli::models::blockstamp::Blockstamp;
use crate::cli::models::proposal::ProposalData;
use crate::display::cli_display::CliDisplay;
use crate::display::spinner::Spinner;
use crate::errors::{ParseError, XandApiError};
use crate::{
    cli::handler::ClientFactory,
    errors::{Result, *},
};

mod data_model_translations;
#[cfg(test)]
mod tests;

pub struct XandApiClientAdapterFactory;

#[async_trait::async_trait]
impl ClientFactory for XandApiClientAdapterFactory {
    type NewClient = XandApiClientAdapter<XandApiClient>;

    async fn new_client(&self, url: String, maybe_jwt: Option<String>) -> Result<Self::NewClient> {
        let url = Url::parse(&url).context(ParseError {
            message: format!("Could not parse URL {}", &url),
        })?;
        let client = if let Some(jwt) = maybe_jwt {
            XandApiClient::connect_with_jwt(&url, jwt).await
        } else {
            XandApiClient::connect(&url).await
        }
        .context(XandApiError {
            message: "Could not connect to Xand API",
        })?;
        Ok(XandApiClientAdapter::new(client))
    }
}

pub struct XandApiClientAdapter<T: XandApiClientTrait + Send + Sync> {
    client: T,
}

impl<T: XandApiClientTrait + Send + Sync> XandApiClientAdapter<T> {
    pub fn new(client: T) -> Self {
        XandApiClientAdapter { client }
    }

    /// Exhausts the transaction status stream
    async fn wait_status_stream(
        &mut self,
        mut stream: TransactionStatusStream,
    ) -> std::result::Result<TransactionUpdate, XandApiClientError> {
        const MAX_POLL_ATTEMPTS: u64 = 30;
        const POLL_INTERVAL_MS: u64 = 1000;
        let mut final_update = TransactionUpdate {
            id: Default::default(),
            status: TransactionStatus::Unknown,
        };
        while let Some(update) = stream.next().await {
            let update = update?;
            CliDisplay::print_line(format!("Received update {:#?}", update.status));
            match update.status {
                TransactionStatus::Invalid(_) | TransactionStatus::Committed => {
                    final_update = update;
                    break;
                }
                _ => {
                    continue;
                }
            }
        }

        CliDisplay::print_line(format!(
            "Checking status of committed transaction: {}",
            final_update.id
        ));
        // Poll the transaction status API to check on the state of network transaction
        let mut transaction = Err(XandApiClientError::NotFound {
            message: "not found".to_string(),
        });
        let mut spinner = Spinner::new(MAX_POLL_ATTEMPTS);
        for _poll_attempt in 0..MAX_POLL_ATTEMPTS {
            spinner.tick(1);
            tokio::time::sleep(Duration::from_millis(POLL_INTERVAL_MS)).await;
            transaction = self.client.get_transaction_details(&final_update.id).await;
            // continue if tx isn't found yet
            match transaction {
                Err(XandApiClientError::NotFound { .. }) => {}
                _ => break,
            }
        }
        spinner.finish();

        match transaction {
            Ok(tx) => match tx.status {
                TransactionStatus::Unknown => Err(XandApiClientError::BadRequest {
                    message: "unknown".to_string(),
                }),
                TransactionStatus::Invalid(reason) => {
                    Err(XandApiClientError::BadRequest { message: reason })
                }
                _ => Ok(TransactionUpdate {
                    id: final_update.id,
                    status: tx.status,
                }),
            },
            Err(e) => Err(e),
        }
    }

    async fn get_participants(
        &self,
    ) -> std::result::Result<(Vec<Address>, Vec<Address>), XandApiClientError> {
        let validators = self.client.get_authority_keys();
        let members = self.client.get_members();
        try_join!(validators, members)
    }
}

#[async_trait::async_trait]
impl<T: XandApiClientTrait + Send + Sync> Client for XandApiClientAdapter<T> {
    async fn submit_proposal(&mut self, issuer: String, proposed_action: Action) -> Result<()> {
        let issuer = Address::try_from(issuer.clone()).map_err(|e| Error::ConversionError {
            message: format!("Issuer address {} invalid: {}", issuer, e),
        })?;
        let stream = self
            .client
            .propose_action(issuer, proposed_action.try_into()?)
            .await
            .context(XandApiError {
                message: "While submitting proposal".to_string(),
            })?;

        self.wait_status_stream(stream)
            .await
            .context(XandApiError {
                message: "While submitting proposal".to_string(),
            })?;
        Ok(())
    }

    async fn submit_vote(&mut self, issuer: String, vote: Vote) -> Result<()> {
        let issuer = Address::try_from(issuer.clone()).map_err(|e| Error::ConversionError {
            message: format!("Issuer address {} invalid: {}", issuer, e),
        })?;
        let stream = self
            .client
            .vote_on_proposal(issuer, vote.into())
            .await
            .context(XandApiError {
                message: "While voting on proposal".to_string(),
            })?;

        self.wait_status_stream(stream)
            .await
            .context(XandApiError {
                message: "While voting on proposal".to_string(),
            })?;
        Ok(())
    }

    async fn get_all_proposals(&self) -> Result<Vec<ProposalData>> {
        let props = self
            .client
            .get_all_proposals()
            .await
            .context(XandApiError {
                message: "While getting all proposals".to_string(),
            })?;
        let (validators, members) = self.get_participants().await.context(XandApiError {
            message: "While getting participants".to_string(),
        })?;

        let mut proposals = Vec::new();
        for prop in props.iter() {
            let proposal_data = build_proposal_data(
                prop,
                Participants {
                    members: members.clone(),
                    validators: validators.clone(),
                },
            )?;
            proposals.push(proposal_data);
        }

        Ok(proposals)
    }

    async fn get_current_block(&self) -> Result<Blockstamp> {
        let current_blockstamp = self
            .client
            .get_current_block()
            .await
            .context(XandApiError {
                message: "While getting current block".to_string(),
            })?;
        Ok(current_blockstamp.into())
    }

    async fn get_proposal(&self, id: u32) -> Result<ProposalData> {
        let prop = self.client.get_proposal(id).await.context(XandApiError {
            message: format!("While getting proposal {}", id),
        })?;
        let (validators, members) = self.get_participants().await.context(XandApiError {
            message: "While getting participants".to_string(),
        })?;
        build_proposal_data(
            &prop,
            Participants {
                members,
                validators,
            },
        )
    }
}
