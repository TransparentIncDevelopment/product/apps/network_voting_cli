use std::convert::TryFrom;

use crate::cli::models::action::Action;
use crate::errors::Error;
use xand_api_client::{
    AddAuthorityKey, AdministrativeTransaction, RegisterAccountAsMember, RemoveAuthorityKey,
    RemoveMember, RootAllowlistCidrBlock, RootRemoveAllowlistCidrBlock, SetCode, SetLimitedAgentId,
    SetTrustNodeId, /* SetValidatorEmissionRate, */
};

impl From<SetCode> for Action {
    fn from(set_code: SetCode) -> Self {
        Action::SetCode {
            code: set_code.code,
        }
    }
}

impl TryFrom<AdministrativeTransaction> for Action {
    type Error = Error;

    fn try_from(txn: AdministrativeTransaction) -> std::result::Result<Self, Self::Error> {
        let action: Result<Action, Error> = match txn {
            AdministrativeTransaction::RegisterAccountAsMember(r) => Ok(r.into()),
            AdministrativeTransaction::RemoveMember(r) => Ok(r.into()),
            AdministrativeTransaction::SetTrust(r) => Ok(r.into()),
            AdministrativeTransaction::SetLimitedAgent(r) => Ok(r.into()),
            AdministrativeTransaction::AddAuthorityKey(r) => Ok(r.into()),
            AdministrativeTransaction::RemoveAuthorityKey(r) => Ok(r.into()),
            AdministrativeTransaction::RootAllowlistCidrBlock(r) => Ok(r.into()),
            AdministrativeTransaction::RootRemoveAllowlistCidrBlock(r) => Ok(r.into()),
            AdministrativeTransaction::SetCode(c) => Ok(c.into()),
            AdministrativeTransaction::SetValidatorEmissionRate(_s) => {
                unimplemented!("temporarily disabled for ADO 6984")
            } // Ok(s.into()),
        };
        action
    }
}

impl TryFrom<Action> for AdministrativeTransaction {
    type Error = Error;

    fn try_from(action: Action) -> std::result::Result<Self, Self::Error> {
        Ok(match action {
            Action::AddMember {
                address,
                encryption_key,
            } => AdministrativeTransaction::RegisterAccountAsMember(RegisterAccountAsMember {
                address: address.parse()?,
                encryption_key: encryption_key
                    .parse()
                    .or(Err(Error::X25519PublicKeyParseError))?,
            }),
            Action::RemoveMember { address } => {
                AdministrativeTransaction::RemoveMember(RemoveMember {
                    address: address.parse()?,
                })
            }
            Action::AddValidator { address } => {
                AdministrativeTransaction::AddAuthorityKey(AddAuthorityKey {
                    account_id: address.parse()?,
                })
            }
            Action::RemoveValidator { address } => {
                AdministrativeTransaction::RemoveAuthorityKey(RemoveAuthorityKey {
                    account_id: address.parse()?,
                })
            }
            Action::SetTrust {
                address,
                encryption_key,
            } => AdministrativeTransaction::SetTrust(SetTrustNodeId {
                address: address.parse()?,
                encryption_key: encryption_key
                    .parse()
                    .or(Err(Error::X25519PublicKeyParseError))?,
            }),
            Action::SetLimitedAgent { address } => {
                AdministrativeTransaction::SetLimitedAgent(SetLimitedAgentId {
                    address: address.map(|a| a.parse()).transpose()?,
                })
            }
            Action::AllowlistCidrBlock {
                cidr_block,
                address,
            } => AdministrativeTransaction::RootAllowlistCidrBlock(RootAllowlistCidrBlock {
                cidr_block: cidr_block
                    .parse()
                    .map_err(|_| Error::CliInvalidCidrBlock { msg: cidr_block })?,
                account: address.parse()?,
            }),
            Action::RemoveAllowlist {
                cidr_block,
                address,
            } => AdministrativeTransaction::RootRemoveAllowlistCidrBlock(
                RootRemoveAllowlistCidrBlock {
                    cidr_block: cidr_block
                        .parse()
                        .map_err(|_| Error::CliInvalidCidrBlock { msg: cidr_block })?,
                    account: address.parse()?,
                },
            ),
            Action::SetCode { code } => AdministrativeTransaction::SetCode(SetCode { code }),
            // temporarily disabled for ADO 6984

            // Action::SetValidatorEmissionRate {
            //     block_quota,
            //     minor_units_per_emission,
            // } => AdministrativeTransaction::SetValidatorEmissionRate(SetValidatorEmissionRate {
            //     minor_units_per_emission,
            //     block_quota,
            // }),
        })
    }
}

impl From<RegisterAccountAsMember> for Action {
    fn from(a: RegisterAccountAsMember) -> Self {
        Action::AddMember {
            address: a.address.to_string(),
            encryption_key: a.encryption_key.to_string(),
        }
    }
}

impl From<RemoveMember> for Action {
    fn from(r: RemoveMember) -> Self {
        Action::RemoveMember {
            address: r.address.to_string(),
        }
    }
}

impl From<SetTrustNodeId> for Action {
    fn from(s: SetTrustNodeId) -> Self {
        Action::SetTrust {
            address: s.address.to_string(),
            encryption_key: s.encryption_key.to_string(),
        }
    }
}

impl From<SetLimitedAgentId> for Action {
    fn from(s: SetLimitedAgentId) -> Self {
        Action::SetLimitedAgent {
            address: s.address.map(|a| a.to_string()),
        }
    }
}

impl From<AddAuthorityKey> for Action {
    fn from(a: AddAuthorityKey) -> Self {
        Action::AddValidator {
            address: a.account_id.to_string(),
        }
    }
}

impl From<RemoveAuthorityKey> for Action {
    fn from(r: RemoveAuthorityKey) -> Self {
        Action::RemoveValidator {
            address: r.account_id.to_string(),
        }
    }
}

impl From<RootAllowlistCidrBlock> for Action {
    fn from(w: RootAllowlistCidrBlock) -> Self {
        Action::AllowlistCidrBlock {
            cidr_block: w.cidr_block.to_string(),
            address: w.account.to_string(),
        }
    }
}

impl From<RootRemoveAllowlistCidrBlock> for Action {
    fn from(r: RootRemoveAllowlistCidrBlock) -> Self {
        Action::RemoveAllowlist {
            cidr_block: r.cidr_block.to_string(),
            address: r.account.to_string(),
        }
    }
}

// temporarily disabled for ADO 6984

// impl From<SetValidatorEmissionRate> for Action {
//     fn from(r: SetValidatorEmissionRate) -> Self {
//         Action::SetValidatorEmissionRate {
//             block_quota: r.block_quota,
//             minor_units_per_emission: r.minor_units_per_emission,
//         }
//     }
// }
