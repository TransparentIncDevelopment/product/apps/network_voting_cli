use std::convert::TryInto;

use xand_api_client::Proposal;

use crate::cli::client::xand_api_client_adapter::data_model_translations::votes;
use crate::cli::client::xand_api_client_adapter::data_model_translations::votes::Participants;
use crate::cli::models::action::Action;
use crate::cli::models::proposal::ProposalData;
use crate::display::cli_display::CliDisplay;
use crate::errors::{Error, Result};

pub(crate) fn build_proposal_data(
    prop: &Proposal,
    participants: Participants,
) -> Result<ProposalData> {
    let participant_votes = votes::build_participant_votes(prop, participants)?;

    let action: Result<Action, Error> = prop.proposed_action.clone().try_into();
    let action = match action {
        Ok(a) => Some(a),
        Err(e) => {
            CliDisplay::print_error(format!(
                "Proposal {} found with invalid action {:?} while parsing action",
                prop.id, prop.proposed_action
            ));
            CliDisplay::print_cli_error(&e);
            None
        }
    };
    Ok(ProposalData {
        id: prop.id,
        action,
        member_votes: participant_votes.members,
        validator_votes: participant_votes.validators,
        status: prop.status.clone().into(),
        proposer: prop.proposer.to_string(),
        expires: prop.expiration_block_id,
    })
}
