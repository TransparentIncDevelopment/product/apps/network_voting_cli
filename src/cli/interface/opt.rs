use structopt::StructOpt;

use crate::cli::interface::cli_action::CliAction;
use crate::cli::interface::vote::Vote;

#[derive(Debug, StructOpt, Clone)]
#[structopt(
    name = "network-voting-cli",
    about = "CLI tool for participating in Xand Network Voting"
)]
pub enum Opt {
    /// List all proposals that have not expired
    List {
        #[structopt(flatten)]
        config_override: ConfigOpts,
        #[structopt(flatten)]
        formatting: FormattingOpts,
    },
    /// Get specific proposal by id
    GetProposal {
        id: u32,
        #[structopt(flatten)]
        config_override: ConfigOpts,
        #[structopt(flatten)]
        formatting: FormattingOpts,
    },
    /// Submit proposal for action
    Propose {
        #[structopt(subcommand)]
        action: CliAction,
        #[structopt(flatten)]
        config_override: ConfigOpts,
        #[structopt(flatten)]
        formatting: FormattingOpts,
    },
    /// Vote on specific proposal by id
    Vote {
        #[structopt(subcommand)]
        vote: Vote,
        #[structopt(flatten)]
        config_override: ConfigOpts,
    },
    /// Set url and issuer default config values
    SetConfig {
        #[structopt(flatten)]
        new_values: ConfigOpts,
        #[structopt(flatten)]
        formatting: FormattingOpts,
    },
    /// Get default config values
    GetConfig {
        #[structopt(flatten)]
        formatting: FormattingOpts,
    },
}

#[derive(StructOpt, Debug, Clone)]
pub struct ConfigOpts {
    #[structopt(short, long)]
    /// Validator address to use for the action
    pub url: Option<String>,
    #[structopt(short, long)]
    /// JWT value to use for the action
    pub jwt: Option<String>,
    #[structopt(short, long)]
    /// Xand address issuing the action
    pub issuer: Option<String>,
}

#[derive(StructOpt, Debug, Clone)]
pub struct FormattingOpts {
    #[structopt(long)]
    /// All Xand addresses will be fully printed
    pub no_truncate: bool,
}
