use std::path::Path;
use std::{fs::write, path::PathBuf};

use config::Config;
use serde::{Deserialize, Serialize};
use snafu::ResultExt;

use crate::errors::{Error, Result, *};

const CONFIG_FILE_SUBPATH: &str = ".config/xand/network.toml";

#[derive(Debug, Deserialize, Serialize)]
pub struct CliConfig {
    pub url: Option<String>,
    pub issuer: Option<String>,
    pub jwt: Option<String>,
}

pub fn save_config_values(
    url: Option<String>,
    issuer: Option<String>,
    jwt: Option<String>,
) -> Result<CliConfig> {
    let cfg = get_default_config_with_overrides(url, issuer, jwt)?;
    write_config_to_file(&cfg)?;
    Ok(cfg)
}

pub fn get_default_config_with_overrides(
    url: Option<String>,
    issuer: Option<String>,
    jwt: Option<String>,
) -> Result<CliConfig> {
    let mut config = get_default_config()?;
    if url.is_some() {
        config.url = url;
    }
    if issuer.is_some() {
        config.issuer = issuer;
    }
    if jwt.is_some() {
        config.jwt = jwt;
    }
    Ok(config)
}

pub fn get_default_config() -> Result<CliConfig> {
    let path = &config_file_path()?;
    let mut config = Config::default();
    // If config file doesn't exist, add one
    if !PathBuf::from(path).exists() {
        write_config_to_file(&CliConfig {
            url: None,
            issuer: None,
            jwt: None,
        })?;
    }
    config
        .merge(config::File::with_name(path))
        .context(ConfigError {
            message: "While reading config",
        })?;
    config.try_into().map_err(|_| Error::CliConfigError {
        message: "Cannot retrieve default config values".to_string(),
    })
}

fn write_config_to_file(cfg: &CliConfig) -> Result<()> {
    let serialized = toml::to_string(&cfg).context(TomlError {
        message: "While serializing cfg",
    })?;
    let path = config_file_path()?;
    let config_dir = Path::new(path.as_str())
        .parent()
        .ok_or(Error::CliConfigError {
            message: format!("Invalid path {}", path),
        })?;
    std::fs::create_dir_all(config_dir).context(IoError {
        message: format!(
            "While creating configuration dir: {:#}",
            config_dir.display()
        ),
    })?;
    write(&path, serialized).context(IoError {
        message: format!("While writing to config file: {}", path),
    })
}

pub fn override_or_default_url(override_url: Option<String>) -> Result<String> {
    let url = if let Some(u) = override_url {
        u
    } else if let Some(u) = get_default_config()?.url {
        u
    } else {
        return Err(Error::CliConfigError {
            message: "Must specify url or set default".to_string(),
        });
    };
    Ok(url)
}

pub fn override_or_default_issuer(override_issuer: Option<String>) -> Result<String> {
    let issuer = if let Some(i) = override_issuer {
        i
    } else if let Some(i) = get_default_config()?.issuer {
        i
    } else {
        return Err(Error::CliConfigError {
            message: "Must specify issuer or set default".to_string(),
        });
    };
    Ok(issuer)
}

// The jwt isn't necessarily required and we shouldn't error because we don't have one.
pub fn maybe_override_or_default_jwt(override_jwt: Option<String>) -> Result<Option<String>> {
    let maybe_jwt = if override_jwt.is_some() {
        override_jwt
    } else {
        get_default_config()?.jwt
    };
    Ok(maybe_jwt)
}

fn config_file_path() -> Result<String> {
    Ok(path_from_home(CONFIG_FILE_SUBPATH)?
        .to_str()
        .ok_or(Error::CliConfigError {
            message: "Cannot convert config file path to str".to_string(),
        })?
        .to_string())
}

fn path_from_home(rel_path: &str) -> Result<PathBuf> {
    let mut path = dirs::home_dir().ok_or(Error::CliConfigError {
        message: "Could not find home directory...?".to_string(),
    })?;
    let rel_path = PathBuf::from(rel_path);
    path.push(rel_path);
    Ok(path)
}
