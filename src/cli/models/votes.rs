#[cfg(test)]
mod tests;

#[derive(Clone, Debug)]
pub struct VotesData {
    pub yea_voters: Vec<String>,
    pub nay_voters: Vec<String>,
}

impl Default for VotesData {
    fn default() -> Self {
        Self::new()
    }
}

impl VotesData {
    pub fn new() -> Self {
        Self {
            yea_voters: Vec::new(),
            nay_voters: Vec::new(),
        }
    }

    pub fn record_vote(&mut self, voter: String, vote: bool) {
        if vote {
            self.yea_voters.push(voter);
        } else {
            self.nay_voters.push(voter);
        }
    }
}
